/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package controlador;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener; 
import javax.swing.JOptionPane;
import javax.swing.JFrame;
import modelo.Docentes;
import vista.dlgDocentes;
/**
 *
 * @author Brandon
 */

public class Controlador1 implements ActionListener{
     private dlgDocentes vista;
    private Docentes doc;
    public Controlador1(Docentes doc, dlgDocentes vista) {
        this.vista = vista;
        this.doc = doc;
      
        vista.btnGuardar.addActionListener(this);
        vista.btnMostrar.addActionListener(this);
        vista.btnNuevo.addActionListener(this); 
        vista.cmbHijos.addActionListener(this);
        vista.btnCancelar.addActionListener(this);
        vista.btnLimpiar.addActionListener(this);
        vista.btnCerrar.addActionListener(this);
    }
    
    public void iniciarVista(){
        vista.setTitle("::ESCUELA::");
        vista.setSize(470, 500);
        vista.setVisible(true);
    }
    public void limpiar(){
        vista.txtNumD.setText("");
        vista.txtNombre.setText("");
        vista.txtDomicilio.setText("");
        vista.spnNivel.setValue("Licenciatura");
        vista.txtPagoHora.setText("");
        vista.txtHoraImp.setText("");
        vista.lblPagoHoras.setText("$");
        vista.lblPagoBono.setText("$");
        vista.lblTotalPag.setText("$");
        vista.lblDesImp.setText("$");
        vista.cmbHijos.setSelectedIndex(0);
    }
    

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Docentes doc = new Docentes();
        dlgDocentes vista = new dlgDocentes(new JFrame(), true);
        Controlador1 contra = new Controlador1(doc, vista);
        contra.iniciarVista();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==vista.btnNuevo){
            vista.txtNumD.setEnabled(true);
            vista.txtNombre.setEnabled(true);
            vista.txtDomicilio.setEnabled(true);
            vista.spnNivel.setEnabled(true);
            vista.txtPagoHora.setEnabled(true);
            vista.txtHoraImp.setEnabled(true);
            vista.lblPagoHoras.setEnabled(true);
            vista.lblPagoBono.setEnabled(true);
            vista.lblTotalPag.setEnabled(true);
            vista.cmbHijos.setEnabled(true);
            vista.btnGuardar.setEnabled(true);
            vista.btnMostrar.setEnabled(true);
            vista.btnLimpiar.setEnabled(true);
            vista.btnCancelar.setEnabled(true);
            vista.lblDesImp.setEnabled(true);
        }
        if(e.getSource()==vista.btnCancelar){
            vista.lblDesImp.setEnabled(false);
            vista.txtNumD.setEnabled(false);
            vista.txtNombre.setEnabled(false);
            vista.txtDomicilio.setEnabled(false);
            vista.spnNivel.setEnabled(false);
            vista.txtPagoHora.setEnabled(false);
            vista.txtHoraImp.setEnabled(false);
            vista.lblPagoHoras.setEnabled(false);
            vista.lblPagoBono.setEnabled(false);
            vista.lblTotalPag.setEnabled(false);
            vista.cmbHijos.setEnabled(false);
            vista.btnGuardar.setEnabled(false);
            vista.btnMostrar.setEnabled(false);
            vista.btnLimpiar.setEnabled(false);
            vista.btnCancelar.setEnabled(false);
            limpiar();
        }
        if(e.getSource()==vista.btnCerrar){
            int option=JOptionPane.showConfirmDialog(vista,"¿Deseas salir?",
            "Decide", JOptionPane.YES_NO_OPTION);
             if(option==JOptionPane.YES_NO_OPTION){
                 vista.dispose();
                 System.exit(0);
             }
        }
        if(e.getSource()==vista.btnGuardar){ 
            try{
                doc.setNombre(vista.txtNombre.getText());
                doc.setDomicilio(vista.txtDomicilio.getText());
                doc.setNumDocentes(Integer.parseInt(vista.txtNumD.getText()));
                if(vista.spnNivel.getValue().toString()=="Licenciatura"){
                   doc.setNivel(0);
                }
                else if(vista.spnNivel.getValue().toString()=="Ingenieria"){
                    doc.setNivel(1);
                }
                else if(vista.spnNivel.getValue().toString()=="Maestro en Ciencias"){
                    doc.setNivel(2);
                }
                else{
                    doc.setNivel(3);
                }
                doc.setHorasIm(Integer.parseInt(vista.txtHoraImp.getText()));
                doc.setPagoB(Float.parseFloat(vista.txtPagoHora.getText()));
            }catch(NumberFormatException ex){
                JOptionPane.showMessageDialog(vista, "Surgio el siguiente error: "+ex.getMessage());
            }
            catch(Exception ex2){
                 JOptionPane.showMessageDialog(vista, "Surgio el siguiente error: "+ex2.getMessage());
            }
             
        }
        if(e.getSource()==vista.btnMostrar){ 
            vista.txtNombre.setText(doc.getNombre());
            vista.txtDomicilio.setText(doc.getDomicilio());  
            vista.txtNumD.setText(Integer.toString(doc.getNumDocentes()));
            vista.txtHoraImp.setText(Integer.toString(doc.getHorasIm())); 
            vista.txtPagoHora.setText(Float.toString(doc.getPagoB())); 
            if(doc.getNivel()==0){
                vista.spnNivel.setValue("Licenciatura");
            }
            else if(doc.getNivel()==1){
                vista.spnNivel.setValue("Ingenieria");
            }
            else if(doc.getNivel()==2){
                vista.spnNivel.setValue("Maestro en Ciencias");
            }
            else{
                vista.spnNivel.setValue("Doctor");
            }
            vista.lblPagoHoras.setText("$ "+doc.CalcularPago());
            vista.lblPagoBono.setText("$ "+doc.CalcularBono((Integer.parseInt(vista.cmbHijos.getSelectedItem().toString()))));
            vista.lblDesImp.setText("$ "+doc.CalcularImp());
            float tot;
            tot=doc.CalcularPago()+doc.CalcularBono((Integer.parseInt(vista.cmbHijos.getSelectedItem().toString())))-doc.CalcularImp();
            vista.lblTotalPag.setText("$ "+tot);
        }
        if(e.getSource()==vista.btnLimpiar){
            limpiar();
        }
        
    }
    
}
