package modelo;

/**
 *
 * @author Brandon
 */
public class Docentes {
    int numDocentes,nivel,horasIm;
    String nombre, domicilio;
    float pagoBaseH;
    
    public Docentes(){
        this.numDocentes=0;
        this.nivel=0;
        this.horasIm=0;
        this.nombre=""; 
        this.domicilio="";
        this.pagoBaseH=0.0f; 
    }
    public Docentes(Docentes doc){
        this.numDocentes=doc.numDocentes;
        this.nivel=doc.nivel;
        this.horasIm=doc.horasIm;
        this.nombre=doc.nombre; 
        this.domicilio=doc.domicilio;
        this.pagoBaseH=doc.pagoBaseH;
    }
    public Docentes(int numDocentes, int nivel,int horasIm, String nombre, String domicilio,  float pagoBaseH){
        this.numDocentes=numDocentes;
        this.nivel=nivel;
        this.horasIm=horasIm;
        this.nombre=nombre; 
        this.domicilio=domicilio;
        this.pagoBaseH=pagoBaseH;
    }
    public void setNumDocentes(int num){
        this.numDocentes=num;
    }
    public int getNumDocentes(){
        return this.numDocentes;
    }
    public void setNivel(int niv){
        this.nivel=niv;
    }
    public int getNivel(){
        return this.nivel;
    }
    public void setHorasIm(int horas){
        this.horasIm=horas;
    }
    public int getHorasIm(){
        return this.horasIm;
    }
    public void setNombre(String nom){
        this.nombre=nom;
    }
    public String getNombre(){
        return this.nombre;
    }
    public void setDomicilio(String dom){
        this.domicilio=dom;
    }
    public String getDomicilio(){
        return this.domicilio;
    }
    public void setPagoB(float pag){
        this.pagoBaseH=pag;
    }
    public float getPagoB(){
        return this.pagoBaseH;
    }
    
    public float CalcularPago(){
        float total=0.0f;
        if(this.nivel==1||this.nivel==0){
            total=(this.pagoBaseH+(this.pagoBaseH/100)*30)*this.horasIm;
        }
        else if(this.nivel==2){
            total=(this.pagoBaseH+(this.pagoBaseH/100)*50)*this.horasIm;
        }
        else{
            total=(this.pagoBaseH*2)*this.horasIm;
        }
        return total;
    }
    public float CalcularImp(){
        float total=0.0f;
        total=(CalcularPago()/100)*16;
        return total;
    }
    public float CalcularBono(int bono){
        float total=0.0f;
        if(bono==0){
            return 0;
        }
        if(bono>=1 && bono<=2){
            total=(CalcularPago()/100)*5;
            return total;
        }
        else if(bono>=3 && bono<=5){
            total=(CalcularPago()/100)*10;
            return total;
        }
        else{
            total=(CalcularPago()/100)*20;
            return total;
        }
    }
}
