/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JDialog.java to edit this template
 */
package vista;

/**
 *
 * @author Brandon
 */
public class dlgDocentes extends javax.swing.JDialog {

    /**

     */
    public dlgDocentes(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel2 = new javax.swing.JLabel();
        txtNumD = new javax.swing.JTextField();
        txtNombre = new javax.swing.JTextField();
        spnNivel = new javax.swing.JSpinner();
        txtPagoHora = new javax.swing.JTextField();
        txtHoraImp = new javax.swing.JTextField();
        txtDomicilio = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        lblTotalPag = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        cmbHijos = new javax.swing.JComboBox<>();
        jLabel12 = new javax.swing.JLabel();
        lblPagoHoras = new javax.swing.JLabel();
        lblPagoBono = new javax.swing.JLabel();
        lblDesImp = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        btnNuevo = new javax.swing.JButton();
        btnMostrar = new javax.swing.JButton();
        btnGuardar = new javax.swing.JButton();
        jPanel8 = new javax.swing.JPanel();
        btnCancelar = new javax.swing.JButton();
        btnLimpiar = new javax.swing.JButton();
        btnCerrar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(null);

        jLabel2.setText("Num Docente");
        getContentPane().add(jLabel2);
        jLabel2.setBounds(20, 20, 74, 16);

        txtNumD.setEnabled(false);
        getContentPane().add(txtNumD);
        txtNumD.setBounds(140, 20, 64, 22);

        txtNombre.setEnabled(false);
        getContentPane().add(txtNombre);
        txtNombre.setBounds(140, 50, 64, 22);

        spnNivel.setModel(new javax.swing.SpinnerListModel(new String[] {"Licenciatura", "Ingenieria", "Maestro en Ciencias", "Doctor"}));
        spnNivel.setEnabled(false);
        getContentPane().add(spnNivel);
        spnNivel.setBounds(140, 110, 100, 22);

        txtPagoHora.setEnabled(false);
        getContentPane().add(txtPagoHora);
        txtPagoHora.setBounds(140, 140, 64, 22);

        txtHoraImp.setEnabled(false);
        getContentPane().add(txtHoraImp);
        txtHoraImp.setBounds(140, 170, 64, 22);

        txtDomicilio.setEnabled(false);
        getContentPane().add(txtDomicilio);
        txtDomicilio.setBounds(140, 80, 64, 22);

        jLabel4.setText("Domicilio");
        getContentPane().add(jLabel4);
        jLabel4.setBounds(20, 80, 51, 16);

        jLabel3.setText("Nombre");
        getContentPane().add(jLabel3);
        jLabel3.setBounds(20, 50, 44, 16);

        jLabel5.setText("Nivel");
        getContentPane().add(jLabel5);
        jLabel5.setBounds(20, 110, 27, 16);

        jLabel6.setText("Pago por Hora base");
        getContentPane().add(jLabel6);
        jLabel6.setBounds(20, 140, 104, 16);

        jLabel7.setText("Horas Impartidas");
        getContentPane().add(jLabel7);
        jLabel7.setBounds(20, 170, 90, 16);

        jPanel1.setBackground(new java.awt.Color(204, 204, 255));
        jPanel1.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jPanel1.setLayout(null);

        jLabel1.setText("Total a Pagar");
        jPanel1.add(jLabel1);
        jLabel1.setBounds(20, 110, 90, 16);

        lblTotalPag.setText("$");
        lblTotalPag.setEnabled(false);
        jPanel1.add(lblTotalPag);
        lblTotalPag.setBounds(200, 110, 150, 16);

        jLabel9.setText("Pago por Bono");
        jPanel1.add(jLabel9);
        jLabel9.setBounds(110, 50, 90, 16);

        jLabel10.setText("Descuento por Impuesto");
        jPanel1.add(jLabel10);
        jLabel10.setBounds(20, 80, 150, 16);

        jLabel11.setText("Hijos");
        jPanel1.add(jLabel11);
        jLabel11.setBounds(20, 50, 40, 16);

        cmbHijos.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1", "2", "3", "4", "5" }));
        cmbHijos.setEnabled(false);
        jPanel1.add(cmbHijos);
        cmbHijos.setBounds(60, 50, 40, 20);

        jLabel12.setText("Pago por horas Impartidas");
        jPanel1.add(jLabel12);
        jLabel12.setBounds(20, 20, 150, 16);

        lblPagoHoras.setText("$");
        lblPagoHoras.setEnabled(false);
        jPanel1.add(lblPagoHoras);
        lblPagoHoras.setBounds(200, 20, 150, 16);

        lblPagoBono.setText("$");
        lblPagoBono.setEnabled(false);
        jPanel1.add(lblPagoBono);
        lblPagoBono.setBounds(200, 50, 150, 16);

        lblDesImp.setText("$");
        lblDesImp.setEnabled(false);
        jPanel1.add(lblDesImp);
        lblDesImp.setBounds(200, 80, 150, 16);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(40, 220, 370, 140);

        jPanel2.setBackground(new java.awt.Color(153, 153, 255));

        jPanel3.setBackground(new java.awt.Color(153, 153, 255));
        jPanel2.add(jPanel3);

        jPanel4.setBackground(new java.awt.Color(153, 153, 255));

        jPanel5.setBackground(new java.awt.Color(153, 153, 255));
        jPanel4.add(jPanel5);

        jPanel2.add(jPanel4);

        getContentPane().add(jPanel2);
        jPanel2.setBounds(0, 0, 450, 10);

        jPanel6.setBackground(new java.awt.Color(153, 153, 255));
        getContentPane().add(jPanel6);
        jPanel6.setBounds(0, 200, 450, 100);

        jPanel7.setBackground(new java.awt.Color(51, 102, 255));
        jPanel7.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel7.setLayout(null);

        btnNuevo.setText("Nuevo");
        btnNuevo.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 255), 2));
        btnNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoActionPerformed(evt);
            }
        });
        jPanel7.add(btnNuevo);
        btnNuevo.setBounds(12, 20, 80, 20);

        btnMostrar.setText("Mostrar");
        btnMostrar.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 255), 2));
        btnMostrar.setEnabled(false);
        btnMostrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMostrarActionPerformed(evt);
            }
        });
        jPanel7.add(btnMostrar);
        btnMostrar.setBounds(12, 100, 80, 20);

        btnGuardar.setText("Guardar");
        btnGuardar.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 255), 2));
        btnGuardar.setEnabled(false);
        jPanel7.add(btnGuardar);
        btnGuardar.setBounds(12, 60, 80, 20);

        getContentPane().add(jPanel7);
        jPanel7.setBounds(320, 30, 100, 150);

        jPanel8.setBackground(new java.awt.Color(102, 102, 255));
        jPanel8.setLayout(null);

        btnCancelar.setText("Cancelar");
        btnCancelar.setEnabled(false);
        jPanel8.add(btnCancelar);
        btnCancelar.setBounds(160, 10, 100, 23);

        btnLimpiar.setText("Limpiar");
        btnLimpiar.setActionCommand("Limpiar");
        btnLimpiar.setEnabled(false);
        btnLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimpiarActionPerformed(evt);
            }
        });
        jPanel8.add(btnLimpiar);
        btnLimpiar.setBounds(30, 10, 72, 23);

        btnCerrar.setText("Cerrar");
        jPanel8.add(btnCerrar);
        btnCerrar.setBounds(310, 10, 72, 23);

        getContentPane().add(jPanel8);
        jPanel8.setBounds(0, 380, 450, 50);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnNuevoActionPerformed

    private void btnLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimpiarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnLimpiarActionPerformed

    private void btnMostrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMostrarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnMostrarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(dlgDocentes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(dlgDocentes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(dlgDocentes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(dlgDocentes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                dlgDocentes dialog = new dlgDocentes(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton btnCancelar;
    public javax.swing.JButton btnCerrar;
    public javax.swing.JButton btnGuardar;
    public javax.swing.JButton btnLimpiar;
    public javax.swing.JButton btnMostrar;
    public javax.swing.JButton btnNuevo;
    public javax.swing.JComboBox<String> cmbHijos;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    public javax.swing.JLabel lblDesImp;
    public javax.swing.JLabel lblPagoBono;
    public javax.swing.JLabel lblPagoHoras;
    public javax.swing.JLabel lblTotalPag;
    public javax.swing.JSpinner spnNivel;
    public javax.swing.JTextField txtDomicilio;
    public javax.swing.JTextField txtHoraImp;
    public javax.swing.JTextField txtNombre;
    public javax.swing.JTextField txtNumD;
    public javax.swing.JTextField txtPagoHora;
    // End of variables declaration//GEN-END:variables
}
